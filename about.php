<?php
include_once('header.php');
?>

<div class="container-full prasathcontent">
   <div class="col-md-6 wrapperleftdiv">
      <div class="col-md-12 leftdiv">
         <div class="col-md-7 col-md-offset-4 abouttext">
            <div class="col-md-12 aboutus">
               About Us
            </div>
            <div class="col-md-12 aboutuspar">
               Did you know that the unemployment rate in the Philippines 
               has raised to 6.6% in January 2017?The main reason behind this rise is the inability to understand the importance of career readiness.Being job ready is one of the biggest hurdles that stops you from joining a company that you’ve been dreaming to associate with. In the world of continuous changes in recruitment, technology, and professions, career readiness differentiates a person’s ability tohave an edge over others to get the perfect job.
            </div>
         </div>
      </div>
   </div>
   <div class="col-md-6 wrapperrightdiv">
      <div class="col-md-12 rightdiv">
         <div class="col-md-6 col-md-offset-2 whytext">
            <div class="col-md-12 whyus">
               Why Us?
            </div>
            <div class="col-md-12 whyuspar">
               <div> The Career Engine encompasses everything that takes you to get hired for the job that suits your aptitude and needs.</div>
               <div class="pardiv">To help you achieve career readiness, we not only aim at assisting you to prepare yourselves to face a job interview but also enable you to find the right job matching with your skillset, and guide you to succeed at your job, once you get hired.</div>
            </div>
         </div>
         <div class="col-md-4">
            &nbsp;
         </div>
      </div>
   </div>
</div>
<div class="container-full">
   <div class="col-md-12 line" >
   </div>
</div>   
<div class="container-full">
   <div class="col-md-12 aboutsection" >
      <!-- <img src="images/prasath.png" /> -->
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12 "> 
               <img class="responsive-image" src="images/second.png">
            </div>
         </div>
      </div>
      <div class="container">
         <div class="row">
            <div class="fulltext">
               <div class="col-sm-4 col-lg-4 col-md-4">
                  <div class="text1">
                     <div class="htext">
                        <h3>Right Job Search</h3>
                     </div>
                     <div class="para">
                        Proin imperdiet in nunc id hendrerit. Sed euismod varius odio sed pulvinar. Phasellus enim odio, tempor eu consec tetur a, ornare non elit.
                     </div>
                     </div>
                     <div class="more">
                        <div class="more1">
                           <p>Read More</p>
                        </div>
                     </div>
                  </div>
               
               <div class="col-sm-4 col-lg-4 col-md-4">
                  <div class="text2">
                     <div class="htext">
                        <h3>Seamless Job Matching</h3>
                     </div>
                     <div class="para">
                        Duis laoreet maximus congue. Mauris vel lectus tempus arcu lobortis efficitur eu nec mi. Curabitur ultrices blandit augue vel rhoncus. Nulla a dui id est bibendum.
                     </div>
                     </div>
                     <div class="more">
                        <div class="more2">
                           <p>Read More</p>
                        </div>
                     </div>
                  </div>
               
               <div class="col-sm-4 col-lg-4 col-md-4">
                  <div class="text3">
                     <div class="htext">
                        <h3>Continuous career growth</h3>
                     </div>
                     <div class="para">
                        Quisque in auctor eros. Aenean ut lacinia urna. In ac eros ac nibh consequat mattis.Vestibulum at commodo nibh. Nunc id arcu interdum, rhoncus urna vitae, venenatis dui.
                     </div>
                     </div>
                     <div class="more">
                        <div class="more3">
                           <p>Read More</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="container-full clearfix">
   
</div>

<?php
include_once('footer.php');
?>