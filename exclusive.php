<?php
   include_once('header.php');
   ?>
<div class="container-full jackcontent">
   <div class="container">
      <div class="homecontenttop col-md-offset-3 col-md-9">
         <h1 class="homepara">The Career Engine Difference</h1>
         <h5 class="homepara1">A Short 1-2 lines of text to briefly introduce the video which can be played below:</h5>
         <div class="video">
            <img src="images/video.png" width="562" height="300">
         </div>
         <h4 class="homepara2">See how Career Engine can help you:</h4>
         <div class="boxes">
            <div class="box1">Become Job Ready</div>
            <div class="box2">Finding the Job</div>
            <div class="box3">Getting the Job</div>
            <div class="box4">Excelling at  the Job</div>
         </div>
      </div>
   </div>
</div>
<div class="container-full">
    <div class="container">
        <div class="col-md-12">
            <h1 class="partner">OUR PARTNERS</h1>
                <h3 class="partnerdesc">a short line to describe the nature of your partnerships</h3>
                <div class="circle">
                <ul>
                    <li class="circle1"></li>
                    <li class="circle2"></li>
                    <li class="circle3"></li>
                    <li class="circle4"></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container-full middlecontent">
    <div class="container">
        <div class="middlecontenttop col-md-12">
            <div class="middle">
                <button class="col-md-3 tablinks active" onclick="openHead(event, 'downpara')"><div class="col-md-3 becomediv">
                  <div class="col-md-12 becomeimg">
                    <img src="images/becomeicon.png">
                  </div> 
                  <div class="col-md-12 becomedesc">
                    Become Job Ready
                  </div>                 
                </div>
                </button>
                <button class="col-md-3 tablinks" onclick="openHead(event, 'downpara1')">
                <div class="col-md-3 finddiv">
                  <div class="col-md-12 becomeimg">
                   <img src="images/findicon.png">
                  </div> 
                  <div class="col-md-12 becomedesc">
                    Finding the Job
                  </div>
                </div>
                </button>
                <button class="col-md-3 tablinks" onclick="openHead(event, 'downpara2')">
                <div class="col-md-3 gettingdiv">
                  <div class="col-md-12 becomeimg">
                    <img src="images/geticon.png">
                  </div> 
                  <div class="col-md-12 becomedesc">
                    Getting the Job
                  </div>
                </div>
                </button>
                <button class="col-md-3 tablinks" onclick="openHead(event, 'downpara3')">
                <div class="col-md-3 excellingdiv">
                  <div class="col-md-12 becomeimg">
                    <img src="images/excelicon.png">
                  </div> 
                  <div class="col-md-12 becomedesc">
                    Excelling at the Job
                  </div>
                </div>
                </button>
            </div>
        </div>
    </div>   
</div>

<div class="container">
    <div id="downpara" class="row tabcontent" style="display: block;">
        <div class="col-md-12 middletext">
            <p>Fusce a turpis quis mi rutrum rhoncus. Etiam ultricies lacus a lectus condimentum, a dapibus est eleifend. Vestibulum efficitur ante vitae dui scelerisque laoreet. Etiam a viverra nibh. Donec metus dui, finibus vitae elementum nec, elementum sed ligula. Sapien ac fringilla tempus, massa velit interdum diam, pharetra ultricies elit orci vel magna. Duis tincidunt sem at congue volut.</p><br>
            <p>Etiam a viverra nibh. Donec metus dui, finibus vitae elementum nec, elementum sed ligula. Sapien ac fringilla tempus, massa velit interdum diam, pharetra ultricies elit orci vel magna. Duis tincidunt sem at congue volutpat. Fusce a turpis quis mi rutrum rhoncus. Etiam ultricies lacus a lectus condimentum, a dapibus est eleifend. Vestibulum efficitur</p><br>
        </div>
    </div>
    <div id="downpara1" class="row tabcontent">
        <div class="col-md-12 middletext">
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia</p>
            <p>Looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.</p>
        </div>
    </div>
    <div id="downpara2" class="row tabcontent">
        <div class="col-md-12 middletext">
            <p>This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
            <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'</p>
        </div>
    </div>
    <div id="downpara3" class="row tabcontent">
        <div class="col-md-12 middletext">
            <p>Fusce a turpis quis mi rutrum rhoncus. Etiam ultricies lacus a lectus condimentum, a dapibus est eleifend. Vestibulum efficitur ante vitae dui scelerisque laoreet. Etiam a viverra nibh. Donec metus dui, finibus vitae elementum nec, elementum sed ligula. Sapien ac fringilla tempus, massa velit interdum diam, pharetra ultricies elit orci vel magna. Duis tincidunt sem at congue volut.</p>
            <p>Making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>
            <p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
        </div>
    </div>
</div>

<div class="container-full">
    <div class="container">
        <div class="col-md-12 bigvideo">
            <img src="images/bigvideo.png" />
        </div>
    </div>
</div>

<div class="container-full">
    <div class="container">
        <div class="col-md-4 module">
            <h3>Module 01:</h3>
                <h1>Communicate your way forward</h1>
                    <h3>Highlighted module summary/ short description will appear here.</h3>
                    <h6>Duration: 15:34 | Rating: <img src="images/star.png"></h6>
        </div>
                   
            
  
                  
   
  </div>
</div>
     


<div class="container-full testi">
    <div class="container">

      <div class="testimonal">
      TESTIMONIALS  
      </div>
      <div class="row">
        <div class="col-md-12" data-wow-delay="0.2s">
          <div class="carousel slide" data-ride="carousel" id="quote-carousel">
            <div class="carousel-inner text-center">
            
            
                <ol class="carousel-indicators">
                    <li data-target="#quote-carousel" data-slide-to="0" class="active"><img class="img-responsive " src="images/minithumbnail1.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="1"><img class="img-responsive" src="images/minithumbnail.png" alt="">
                    </li>
                    <li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="images/minithumbnail2.png" alt="">
                    </li>
                    </ol>
                  <div class="item active">
                    <div class="row">
                      <div class="col-sm-8 col-sm-offset-2">

                          <p data-target="#quote-carousel" data-slide-to="0" class="active ">"Nulla dapibus semper orci sit amet interdum. Aliquam ultricies imperdiet ante, sed cursus quam dapibus at. Donec pretium, dolor nec elementum aliquet, leo eros sodales nulla, ac cursus nunc eros id nibh."</p>
                                   
                        </div>
                      </div>
                    </div>
                        <div class="item" >
                          <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">

                              <p data-target="#quote-carousel" data-slide-to="1" class="active ">Donec pretium, dolor nec elementum aliquet, leo eros sodales nulla, ac cursus nunc eros id nibh. Nulla dapibus semper orci sit amet interdum. Aliquam ultricies imperdiet ante, sed cursus quam dapibus at.</p>
                                    
                            </div>
                          </div>
                        </div>
                            <div class="item">
                              <div class="row">
                                <div class="col-sm-8 col-sm-offset-2">
                                    <p data-target="#quote-carousel" data-slide-to="2" class="active ">Leo eros sodales nulla, ac cursus nunc eros id nibh, Nulla dapibus semper orci sit amet interdum. Aliquam ultricies imperdiet ante, sed cursus quam dapibus at. Donec pretium, dolor nec elementum aliquet.</p>
                                 
                                </div>
                              </div>
                            </div>
                          

                         <div class="juana">Juana dela Cruz</div>
                         <div class="subject">IT Specialist, 22</div>  

                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
            </div>
          </div>
      </div>
    </div>                
  </div>
</div>
<div class="container-full bottomcontent">
    <div class="container">
        <div class="event">EVENTS</div>
            <div class="col-md-offset-4 col-md-8"></div>
            <div class="event1">Two to three sentences to describe what kind of events Career Engine holds <br>and what people can expect from them
        </div>
    </div>


    <div class="container">
        <div class="col-sm-4"> 
            <img src="images/bot1.png">

        </div>
                <div class= "col-md-4">
                    <img src="images/bot2.png">
                    </div>
                    <div class= "col-md-4">
                <img src="images/bot3.png">
            </div>
        
    </div>
</div>

 <div class="date">Jul </div>
  <div class="day">21</div>
    <div class="eventtitle"><h1>Event Title</h1></div>
        <div class="eventpara">
        Two to three sentences to describe what kind of events Career Engine holds and what people can expect from them.
        </div>
        <div class="read"><a href="#">read more...</a></div>
    </div>

<div class="date1">Jul</div>
    <div class="day1">26</div>
    <div class="eventtitle1"><h1>Event Title</h1>
        </div>
        <div class="eventpara1">
        Two to three sentences to describe what kind of events Career Engine holds and what people can expect from them.
        </div>
    <div class="read1"><a href="#">read more...</a>
</div>
<div class="date2">Aug</div>
    <div class="day2">12</div>
    <div class="eventtitle2"><h1>Event Title</h1>
    </div>
        <div class="eventpara2">
        <p>Two to three sentences to describe what kind of events Career Engine holds and what people can expect from them.
        </p>
    </div>
    <div class="read2"><a href="#">read more...</a>
</div> 
      </div>
    </div>
  </div>
</div>

<?php
   include_once('footer.php');
   ?>