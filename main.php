<?php
include_once('header.php');
?>


        <div class="container-full bannerimg">
			<div class="container">
				<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
					<div class="col-md-4 col-md-offset-2 col-lg-4 col-lg-offset-2 col-sm-4 col-sm-offset-2 col-xs-4 col-xs-offset-2">
						<img src="images/bannericon.png">	
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
						<h1 class="bannertext">Finding the Job</h1>	
					</div>
				</div>
			</div>	
		</div>
		<div class="container innercontainer">
			<div class="row mymodule">
				<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
					<div class="redtext"><u>The Right Job Channels for You</u></div>
					<div class="greybar">|</div>
					<div class="greytext">The Right Company for You</div>
				</div>
			</div>		
			<div class="row">
				<div class="col-md-12 videop">
					<img src="images/videop.jpg">
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-7 col-md-5 col-lg-offset-7 col-lg-5 col-xs-12 col-sm-12 videodesc">
					<div>Duration: 15:34  |  Rating: &nbsp; &nbsp;<img src="images/rating.png" /></div>
				</div>
			</div>
			<div class="row tabmedia">
				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 synopsis">
				<button class="tablinks active" onclick="openHead(event, 'synopsis')"><img src="images/synopsis.png"><br>Synopsis</button>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 whatuwill">
				<button class="tablinks" onclick="openHead(event, 'whatuwill')"><img src="images/whatuwill.png"><br>What You&#8217;ll Learn</button>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 download">
				<button class="tablinks" onclick="openHead(event, 'download')"><img src="images/download.png"><br>Download</button>
				</div>
			</div>
			<div id="synopsis" class="row tabcontent" style="display: block;">
				<div class="col-md-12 desctext">
					<p>Fusce a turpis quis mi rutrum rhoncus. Etiam ultricies lacus a lectus condimentum, a dapibus est eleifend. Vestibulum efficitur ante vitae dui scelerisque laoreet. Etiam a viverra nibh. Donec metus dui, finibus vitae elementum nec, elementum sed ligula. Sapien ac fringilla tempus, massa velit interdum diam, pharetra ultricies elit orci vel magna. Duis tincidunt sem at congue volut.</p><br>
					<p>Etiam a viverra nibh. Donec metus dui, finibus vitae elementum nec, elementum sed ligula. Sapien ac fringilla tempus, massa velit interdum diam, pharetra ultricies elit orci vel magna. Duis tincidunt sem at congue volutpat. Fusce a turpis quis mi rutrum rhoncus. Etiam ultricies lacus a lectus condimentum, a dapibus est eleifend. Vestibulum efficitur</p><br>
					<p>Fusce a turpis quis mi rutrum rhoncus. Etiam ultricies lacus a lectus condimentum, a dapibus est eleifend. Vestibulum efficitur ante vitae dui scelerisque laoreet. Etiam a viverra nibh. Donec metus dui, finibus vitae elementum nec, elementum sed ligula. Sapien ac fringilla tempus, massa velit interdum diam, pharetra ultricies elit orci vel magna. Duis tincidunt sem at congue volut.</p>
				</div>
			</div>
			<div id="whatuwill" class="row tabcontent">
				<div class="col-md-12 desctext">
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
					<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia</p>
					<p>Looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC.</p>
				</div>
			</div>
			<div id="download" class="row tabcontent">
				<div class="col-md-12 desctext">
					<p>This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
					<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here'</p>
					<p>Making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>
					<p>Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
				</div>
			</div>
		</div>
		<div class="container-full bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-12 queries">
						QUERIES?
					</div>		
					<div class="col-md-12 botttext">
						Fusce a turpis quis mi rutrum rhoncus. Etiam ultricies lacus a lectus condimentum, a dapibus est eleifend. Vestibulum efficitur ante vitae dui scelerisque laoreet. Etiam a viverra nibh. Donec metus dui, finibus vitae elementum nec, elementum sed ligula.
					</div>						
				</div>
				<div class="row">
					<div class="col-md-5">
						<div class="col-md-12 inputname">
							<input type="text" name="Name" placeholder="Name">
						</div>
						<div class="col-md-12 inputemail">
							<input type="text" name="Email" placeholder="Email">
						</div>
						<div class="col-md-12 buttonsend hidden-xs hidden-sm">
							&nbsp;
						</div>
						<div class="col-md-12 buttonsend hidden-xs hidden-sm">
							<input type="submit" value="SEND">
						</div>
						
					</div>
					<div class="col-md-7">	
						<div class="col-md-12 rtextarea">
							<textarea>
							</textarea>
						</div>
					</div>
					<div class="col-md-5">
						<div class="col-md-12 buttonsend hidden-md hidden-lg">
							<input type="submit" value="SEND">
						</div>						
					</div>
				</div>
			</div>	
		</div>


		<script>

		//tab
		function openHead(evt, topicName) {
		    var i, tabcontent, tablinks;
		    tabcontent = document.getElementsByClassName("tabcontent");
		    for (i = 0; i < tabcontent.length; i++) {
		        tabcontent[i].style.display = "none";
		    }
		    tablinks = document.getElementsByClassName("tablinks");
		    for (i = 0; i < tablinks.length; i++) {
		        tablinks[i].className = tablinks[i].className.replace(" active", "");
		    }
		    document.getElementById(topicName).style.display = "block";
		    evt.currentTarget.className += " active";
		}

		</script>
    

<?php
include_once('footer.php');
?>