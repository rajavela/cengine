<?php
include_once('header.php');
?>

<!--BANNER-->
<div class="container-full banner">
	

        <div class="container bannercontent">
        		<div class="contenttop col-md-offset-6 col-md-6">
                       <h1>What Our  Employers Love</h1>
                       <p>“Nulla dapibus semper orci sit amet interdum. Aliquam  ultricies imperdiet ante, sed cursus quam dapibus at. Donec pretium, dolor nec elementum aliquet, leo eros sodales nulla, ac cursus nunc eros id nibh."</p>
                       <h3>Juana dela Cruz</h3>
                       <h6>HR Director, Fortune 500 Company</h6>
        		</div>

        		<!--form-->
        		<div class="contentform col-md-offset-6 col-md-6 col-xs-offset-4 col-xs-8">
        			<h3 class="formtitle">Find your next hire here:</h3>
        			<form action="/action_page.php">
                         <br>
                          <span class="input-addon" id="basic-addon1"><img src="images/formname.png"></span>
                          <input type="text" class="form-control" placeholder="Name" aria-describedby="basic-addon1">
                          <br>
                          <span class="input-addon" id="basic-addon1"><img src="images/formcompany.png"></span>
                          <input type="text" class="form-control" placeholder="Company" aria-describedby="basic-addon1">
                          <br>
                          <span class="input-addon" id="basic-addon1"><img src="images/formjob.png"></span>
                          <input type="text" class="form-control" placeholder="Job Description" aria-describedby="basic-addon1">
                          <br>

                          <select name="selCandidateprofile" id="selCandidateprofile" class="selCandidateprofile">
                            <option> Candidate profile1</option>
                            <option> Candidate profile2</option>
                            <option> Candidate profile3</option>
                            <option> Candidate profile4</option>
                          </select>
                    </form>
        		</div>
        </div>

</div>


<!--CONTENT-->
<div class="container">
        <div class="content">
            <div class="row">
                <div class="contenthead text-center">
                    <h1>WHY HIRE YOUR NEXT EMPLOYEE FROM US?</h1>
                </div>
            <!-- /.col-lg-12 -->
            </div>
        <!-- /.row -->

        <!-- Content Row -->
            <div class="row ">
                     <div class="col-md-4 contentmiddle">
                        <img src="images/logo1.png">
                        <h2>Hands On Learning</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor consectetur.</p>
                        <div class="buttom">
                        	<a class="btm btm-default-new" href="#">Read More</a>
                        </div>
                    </div>
                <!-- /.col-md-4 -->
                    <div class="col-md-4 contentmiddle">
                        <img src="images/logo2.png">
                        <h2>Real Time Training</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor consectetur.</p>
                        <div class="buttom">
                        	<a class="btn btm-default-new" href="#">Read More</a>
                        </div>
                    </div>
                <!-- /.col-md-4 -->
                    <div class="col-md-4 contentmiddle">
                        <img src="images/logo3.png">
                        <h2>Zero Cost for Hiring</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe rem nisi accusamus error velit animi non ipsa placeat. Recusandae, suscipit, soluta quibusdam accusamus a veniam quaerat eveniet eligendi dolor consectetur.</p>
                        <div class="buttom">
                        	<a class="btn btm-default-new" href="#">Read More</a>
                        </div>
                    </div>
                <!-- /.col-md-4 -->
            </div>
        </div>
</div>

<script type="text/javascript">
  $('.selCandidateprofile').selectpicker();
</script>
    

<?php
include_once('footer.php');
?>	