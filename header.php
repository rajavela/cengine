<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Career Engine</title>

    <!-- Bootstrap Core CSS -->
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" media="all and (device-width: 768px) and (device-height: 1024px) and (orientation:portrait)" href="ipad-portrait.css" />
    <link rel="stylesheet" media="all and (device-width: 768px) and (device-height: 1024px) and (orientation:landscape)" href="ipad-landscape.css" />

     <!-- Custom CSS -->
    <link href="css/style.css" type="text/css" rel="stylesheet">
    <link href="css/prasath.css" type="text/css" rel="stylesheet">
    <link href="css/parthi.css" type="text/css" rel="stylesheet">
    <link href="css/ganesh.css" type="text/css" rel="stylesheet">
    <link href="css/jack.css" type="text/css" rel="stylesheet">
    <link href="css/nisha.css" type="text/css" rel="stylesheet">
    <link href="css/karthifooter.css" type="text/css" rel="stylesheet">

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    
    <script src="js/myjs.js" type="text/javascript"></script>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div class="container-full topheader">
    <div class="container" style="border:0px solid green;">
        Contact us: info@careerengine.org.ph  |  +63 912 345 6789
    </div>
</div>

<div class="container-full wrapperclass">
    <div class="col-md-offset-7 col-md-5 col-sm-offset-5 col-sm-7 col-xs-offset-1  col-xs-11 col-lg-offset-6 col-lg-6 wrapperinner">
        <div class="col-md-1 col-xs-1 wrappercutinnerfirst">
            &nbsp;
        </div>
        <div class="col-md-11 col-xs-11 wrappercutinner">
            <ul>
                <!-- <li class="cutimageclass"><img src="images/login.png"></li>
                <li><a href="#">Login</a></li> -->
                <li><a href="#" onclick="document.getElementById('id01').style.display='block'" style="width:auto;"><img src="images/login.png"></a></li>
                <li><a href="#" onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Login</a></li>
                <li>|</li>
                <li><a   data-toggle="modal" data-target="#myModal">
    start Here</a></li>
                <li>|</li>
                <li><a href="#">Language</a></li>
            </ul>
        </div>
    </div>
</div>



<!-- ganesh -->

<div class="container myheader">
    <div class="col-md-5 col-sm-12 col-xs-12 myheadersecond">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <img src="images/logo.png">
        </div>
        <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-3 col-xs-9 col-md-offset-3 col-md-6 logotext">
            Powered by DigiBayanihan
        </div>
    </div>
    <div class="col-md-offset-1 col-md-6">
        <nav class="navbar navbar-inverse mynav">
          
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
              </button>
            </div>
            <div class="collapse navbar-collapse mylist" id="myNavbar">
              <ul class="nav navbar-nav">
                  <li class="activenav"><a href="#">HOME</a></li>
                  <li><a href="#">ABOUT US</a></li>
                  <li><a href="#">OUR COURSES</a></li>
                  <li><a href="#">BUSINESS</a></li>
                  <li><a href="#">EVENTS</a></li>
                  <li><a href="#"><img src="images/search.png"></a></li>
                  <li><a href="#"><img src="images/upload.png"></a></li>
                  <li><a href="#">SHARE</a></li>
                </ul>
            </div>
          
        </nav>
    </div>
    
</div>



<!-- Kalaiyarasan signup -->

<!-- Large modal -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true" >
    <div class="modal-dialog modal-lg" >
        <div class="modal-content" style=" background-color:  #333366;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <h4 class="text-center" id="myModalLabel"><a href="#Registration" data-toggle="tab">
                    Signup</a></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="First Name" tabindex="1">
                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2">
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <select  class="form-control" placeholder="Kalaiyarasan" >

                                    <option>Mr.</option>
                                    <option>Ms.</option>
                                    <option>Mrs.</option>

                                </select>
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <select  class="form-control" placeholder="Kalaiyarasan" >

                                    <option>Mr.</option>
                                    <option>Ms.</option>
                                    <option>Mrs.</option>

                                </select>
                            </div>
                    </div>

                    

                    <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2">
                            </div>
                    </div>


                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="First Name" tabindex="1">
                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2">
                            </div>
                    </div>

                    
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="form-group">
                        <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
                        </div>
                    </div>



                    
                    
                       
                           
                  







                    






                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="First Name" tabindex="1">
                            </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2">
                            </div>
                    </div>
                                                                <!-- Nav tabs -->
                        <!-- <ul class="nav nav-tabs">
                            <li class="active"><a href="#Login" data-toggle="tab">Login</a></li>
                            <li><a href="#Registration" data-toggle="tab">Registration</a></li>
                        </ul> -->
                        <!-- Tab panes -->
                        <!-- <div class="tab-content"> -->
                            <!-- <div class="tab-pane active" id="Login">
                                <form role="form" class="form-horizontal">
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">
                                        Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="email1" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                        Password</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" id="exampleInputPassword1" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            Submit</button>
                                        <a href="javascript:;">Forgot your password?</a>
                                    </div>
                                </div>
                                </form>
                            </div> -->
                            <div class="tab-pane" id="Registration">
                                <form role="form" class="form-horizontal">
                                sgssgsgsdgsg
                                </form>
                            <!-- </div> -->
                        </div>
                        
                    
                </div>
            </div>
        </div>
    </div>
</div>




<!-- end login -->
